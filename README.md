## 📺 Giới thiệu
Personal Tasks Management Mobile - Hệ thống Quản lý công việc cá nhân Mobile - Hỗ trợ chế độ Offline (Không cần sử dụng mạng).
## Chức Năng
### Habit
#### Tạo thói quen

<img src="https://user-images.githubusercontent.com/30486505/127100680-0898c606-8a6a-467d-b492-6c4842f3e0fa.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127100689-4f826a6f-bd02-4a40-9542-2ca02577c9ae.jpg" width="150px"/>

#### Chi tiết thói quen
<img src="https://user-images.githubusercontent.com/30486505/127101369-eee77241-d3f9-4f97-b661-672277646728.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127101372-6a57acc9-900e-412e-a615-8ac9cb6d465c.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127101374-572904bc-ff23-440f-846d-8ae21bf78cc8.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127101375-880de4a0-3321-4758-8849-4b6905472739.jpg" width="150px"/>

#### Danh sách thói quen
<img src="https://user-images.githubusercontent.com/30486505/127101646-4c5d61e9-ddff-4234-9954-2dfa174d8ed0.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127101650-9f3e6de8-8325-42b6-bd10-019ceb89e864.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127101654-b4a0e688-85c3-4374-b145-08fb38a3eda6.jpg" width="150px"/>

#### Nhật ký
<img src="https://user-images.githubusercontent.com/30486505/127101925-4fc7b52c-b267-4b5a-b953-115c857e4f02.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127101939-fd60cf89-3ac0-4b4c-a1f6-ef0a2fcfabd7.jpg" width="150px"/>

### Task
#### Tạo Task, Project, Label
<img src="https://user-images.githubusercontent.com/30486505/127102070-1d5b39ef-24c7-4aef-a4c2-f4489fb1a438.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127102075-52840678-b1bd-4a76-9b88-393f2b2ab791.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127102079-710650d3-f431-4cf3-8977-83f6a40808a6.jpg" width="150px"/>

#### Danh sách Task
<img src="https://user-images.githubusercontent.com/30486505/127102387-ebb559b8-65b0-458b-bb66-e2be4ab5d0a0.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127102392-66a6aa55-828c-4e34-b1e7-5fd35b1430f8.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127102395-9a909081-a4b5-467f-9686-9a1882478b16.jpg" width="150px"/>

#### Thống kê
<img src="https://user-images.githubusercontent.com/30486505/127102467-53bda397-4a21-471f-8b39-9698275d16ab.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127102474-182dcee1-5a70-4f71-9189-bc0a8971a59c.jpg" width="150px"/> 

#### Drawer
<img src="https://user-images.githubusercontent.com/30486505/127102558-9d308999-7724-4cf3-a2c0-df7f96a7d996.jpg" width="150px"/> <img src="https://user-images.githubusercontent.com/30486505/127102565-394c6d88-636f-4cfa-9b23-9eac237ea08c.jpg" width="150px"/> 

#### Chi tiết Task
<img src="https://user-images.githubusercontent.com/30486505/127102668-e5ac149c-4de2-4c66-a28e-6fc7ea84ba8d.jpg" width="150px"/>

## 📌 Link Youtube demo
Link: https://youtu.be/ZHPOk45BwWU
## 🔒 Quyền sử dụng Project
Có thể share cho các khóa sau

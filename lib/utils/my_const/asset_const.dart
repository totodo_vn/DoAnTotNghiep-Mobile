const String kIconToday = "assets/ic_today_64.png";
const String kIconDashboard = "assets/ic_dashboard_64.png";
const String kIconLabel = "assets/ic_label_64.png";
const String kIconCircle = "assets/ic_circle_64.png";

const String kImageMotivation = "assets/habit/motivation.jpg";
// const String kImageMotivationGym = "assets/habit/motivation_gym.jpg";
// const String kImageMotivationArm = "assets/habit/motivation_arm.jpg";
const String kImageTag = "assets/habit/ic_tag_256.png";

const String kBackgroundDiary = "assets/background_diary.jpg";

const String kIconHappy = "assets/emotional/ic_happy.png";

// Habit
// const String kIconMeditation = "assets/habit/ic_meditation_64.png";
// const String kIconPushUp = "assets/habit/ic_push_up_64.png";
// const String kBike = "assets/habit/ic_bike_64.png";
// const String kBook = "assets/habit/ic_book_64.png";
// const String kBreakfast = "assets/habit/ic_breakfast_64.png";
// const String kCash = "assets/habit/ic_cash_64.png";
// const String kDiary = "assets/habit/ic_diary_64.png";
// const String kKaraoke = "assets/habit/ic_karaoke_64.png";
// const String kQuotation = "assets/habit/ic_quotation_64.png";
// const String kSleep = "assets/habit/ic_sleep_64.png";
// const String kSun = "assets/habit/ic_sun_64.png";
// const String kSwimming = "assets/habit/ic_swimming_64.png";
// const String kWalking = "assets/habit/ic_walking_64.png";
// const String kWater = "assets/habit/ic_water_64.png";

import 'package:flutter/foundation.dart';

class ItemDataStaticDetailHabit {
  final String dayOfMonth;
  final double amountUnit;

  const ItemDataStaticDetailHabit({
    @required this.dayOfMonth,
    @required this.amountUnit,
  });
}
